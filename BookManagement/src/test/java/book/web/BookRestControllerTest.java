package book.web;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import com.book.configuration.Application;
import com.book.model.Book;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class BookRestControllerTest {
	
	@LocalServerPort
	private int port;

	Book mockBook = new Book.Builder().bookId("1").title("Book Title 1").author("Author 1").publisher("Publisher").build();

	TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();
	
	
	@Test
	public void getBookRestTest() throws JSONException {

		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(
			  createURLWithPort("/bookManagement/book/getBook/1"), HttpMethod.GET, entity,
			  String.class);
		 
		String expected = "{\"bookId\":\"1\",\"title\":\"Book Title 1\",\"author\":\"Author 1\",\"publisher\":\"Publisher\"}";

		JSONAssert.assertNotEquals(expected, response.getBody(), false);
	}
	
	@Test
	public void saveBook() {

		Book book = new Book.Builder().bookId("3").title("Book Title 2").author("Author 2").publisher("Publisher").build();

		HttpEntity<Book> entity = new HttpEntity<Book>(book, headers);

		ResponseEntity<Book> response = restTemplate.exchange(
				createURLWithPort("/bookManagement/book/saveBook"),
				HttpMethod.POST, entity, Book.class);
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
}

package com.book.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.book.model.Book;

@Repository
public class BookDAO implements IBookDAO{

	private List<Book> bookList = new ArrayList<Book>();

	private List<Book> getAllBooks() {
		bookList.add(new Book.Builder().bookId("1").title("Book Title 1").author("Author 1").publisher("Publisher").build());
		bookList.add(new Book.Builder().bookId("2").title("Book Title 2").author("Author 2").publisher("Publisher").build());
		return bookList;
	}

	public Book getBookById(String bookId) {
		this.getAllBooks();
		for( Book book : bookList) {
			if(bookId.equals(book.getBookId())) {
				return book;
			}
		}
		return null;
	}

	public Book saveBook(Book newBook) {
		this.getAllBooks();
		for( Book book : bookList) {
			if(newBook.getBookId().equals(book.getBookId())) {
				book.setTitle(newBook.getTitle());
				book.setAuthor(newBook.getAuthor());
				book.setPublisher(newBook.getPublisher());
				return book;
			}
		}
		bookList.add(newBook);
		return newBook;
	}

}

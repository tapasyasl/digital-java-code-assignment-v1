package com.book.persistence;

import com.book.model.Book;

public interface IBookDAO {
	
	public Book getBookById(String bookId);
	
	public Book saveBook(Book newBook);
		
}

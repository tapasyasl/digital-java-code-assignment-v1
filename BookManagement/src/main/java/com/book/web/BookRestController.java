package com.book.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.book.business.IBookService;
import com.book.model.Book;


@RestController
@RequestMapping(value="/book")
public class BookRestController {
	
	 @Autowired
	 private IBookService bookService;
	 
	 @RequestMapping(value = "/getBook/{bookId}", method = RequestMethod.GET)
	    public Book getBook(@PathVariable String bookId) {
		     return this.bookService.getBookById(bookId);
	    }
	 
	 @RequestMapping(value = "/saveBook", method = RequestMethod.POST)
	    public Book saveBook(@RequestBody Book book) {
		      return this.bookService.saveBook(book);
	 }

}

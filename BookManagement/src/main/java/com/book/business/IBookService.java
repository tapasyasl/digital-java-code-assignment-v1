package com.book.business;

import com.book.model.Book;

public interface IBookService {

	public Book getBookById(String bookId);
	
	public Book saveBook(Book book);
}

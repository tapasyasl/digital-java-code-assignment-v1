package com.book.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.book.model.Book;
import com.book.persistence.IBookDAO;

@Service
public class BookService implements IBookService{
	
	private final IBookDAO bookDAO;
	
	@Autowired
	public BookService(final IBookDAO bookDAO) {
		this.bookDAO = bookDAO;
	}

	public Book getBookById(String bookId) {
		return bookDAO.getBookById(bookId);
	}

	public Book saveBook(Book book) {
		return bookDAO.saveBook(book);
	}

}

package com.book.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;


@XmlRootElement
public class Book {
	
	@NotNull
	private String bookId;
	@NotNull
	private String title;
	private String author;
	private String publisher;
	
	
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	@Override
	public String toString() {
		return String.format(
				"bookId:"+bookId, "title:"+title, "author:"+author, "publisher:"+publisher);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookId == null) ? 0 : bookId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (bookId == null) {
			if (other.bookId != null)
				return false;
		} else if (!bookId.equals(other.bookId))
			return false;
		return true;
	}
	
	public static class Builder{
		private String bookId;
		private String title;
		private String author;
		private String publisher;
		
		public Builder bookId(final String bookId) {
            this.bookId = bookId;
            return this;
        }
		
		public Builder title(final String title) {
            this.title = title;
            return this;
        }
		
		public Builder author(final String author) {
            this.author = author;
            return this;
        }
		
		public Builder publisher(final String publisher) {
            this.publisher = publisher;
            return this;
        }
		
		public Book build() {
			final Book book = new Book();
			book.bookId = this.bookId;
			book.title = this.title;
			book.author = this.author;
			book.publisher = this.publisher;
			return book;
		}
	}
	
}
